/*
    SPDX-FileCopyrightText: 2023 Oliver Beard <olib141@outlook.com>
    SPDX-License-Identifier: MIT
*/

import QtQuick
import QtQuick.Layouts
import QtQuick.Controls as QQC2

import org.kde.kcmutils as KCM
import org.kde.kirigami as Kirigami

KCM.SimpleKCM {

    property bool cfg_showUnit
    property bool cfg_enableDangerColor
    property int cfg_warningThreshold
    property int cfg_meltdownThreshold
    property bool cfg_swapLabels
    property bool cfg_isoFont
    property double cfg_fontScale
    property double cfg_secondaryFontScale
    property double cfg_secondaryLabelOpacity
    property bool cfg_secondaryLabelCentered

    // HACK: Present to suppress errors
    property string cfg_sensors
    property string cfg_sensorsDefault
    property bool cfg_showUnitDefault
    property bool cfg_enableDangerColorDefault
    property int cfg_warningThresholdDefault
    property int cfg_meltdownThresholdDefault
    property bool cfg_swapLabelsDefault
    property bool cfg_isoFontDefault
    property double cfg_fontScaleDefault
    property double cfg_secondaryFontScaleDefault
    property double cfg_secondaryLabelOpacityDefault
    property bool cfg_secondaryLabelCenteredDefault
    property double cfg_updateInterval
    property double cfg_updateIntervalDefault
    property int cfg_temperatureUnit
    property int cfg_temperatureUnitDefault

    onCfg_showUnitChanged: { showUnitBox.checked = cfg_showUnit; }
    onCfg_enableDangerColorChanged: { enableDangerColorBox.checked = cfg_enableDangerColor; }
    onCfg_warningThresholdChanged: { warningThresholdSpinBox.value = cfg_warningThreshold; }
    onCfg_meltdownThresholdChanged: { meltdownThresholdSpinBox.value = cfg_meltdownThreshold; }
    onCfg_swapLabelsChanged: {
        primaryLabelTemperatureButton.checked = !cfg_swapLabels;
        primaryLabelNameButton.checked = cfg_swapLabels;
    }
    onCfg_isoFontChanged: { isoFontBox.checked = cfg_isoFont; }
    onCfg_fontScaleChanged: { primaryFontScaleSpinBox.value = primaryFontScaleSpinBox.toInt(cfg_fontScale); }
    onCfg_secondaryFontScaleChanged: { secondaryFontScaleSpinBox.value = secondaryFontScaleSpinBox.toInt(cfg_secondaryFontScale); }
    onCfg_secondaryLabelOpacityChanged: { secondaryLabelOpacitySpinBox.value = secondaryLabelOpacitySpinBox.toInt(cfg_secondaryLabelOpacity); }
    onCfg_secondaryLabelCenteredChanged: { secondaryLabelCenteredBox.checked = cfg_secondaryLabelCentered; }

    Component.onCompleted: cfg_swapLabelsChanged()

    Kirigami.FormLayout {

        QQC2.CheckBox {
            id: showUnitBox
            Kirigami.FormData.label: "Temperature:"

            text: "Show unit"
            onCheckedChanged: { cfg_showUnit = checked; }
        }

        QQC2.CheckBox {
            id: enableDangerColorBox

            text: "Enable danger color"
            onCheckedChanged: { cfg_enableDangerColor = checked; }
        }

        QQC2.Label {
            Layout.fillWidth: true

            leftPadding: enableDangerColorBox.indicator.width
            text: "Change the color of the temperature label"
            textFormat: Text.PlainText
            elide: Text.ElideRight
            font: Kirigami.Theme.smallFont
        }

        RowLayout {
            QQC2.Label {
                Layout.leftMargin: enableDangerColorBox.indicator.width
                text: "Warning threshold:"
            }

            QQC2.SpinBox {
                id: warningThresholdSpinBox

                enabled: cfg_enableDangerColor

                stepSize: 1
                from: 0
                to: 150

                validator: IntValidator {
                    bottom: warningThresholdSpinBox.from
                    top: warningThresholdSpinBox.to
                }

                textFromValue: (value, locale) => {
                    return Number(value).toLocaleString(locale, 'f', 0) + " °C";
                }

                valueFromText: (text, locale) => {
                    return Number.fromLocaleString(locale, text.split(" ")[0]);
                }

                onValueChanged: { cfg_warningThreshold = value; }
            }
        }

        RowLayout {
            QQC2.Label {
                Layout.leftMargin: enableDangerColorBox.indicator.width
                text: "Meltdown threshold:"
            }

            QQC2.SpinBox {
                id: meltdownThresholdSpinBox

                enabled: cfg_enableDangerColor

                stepSize: 1
                from: 0
                to: 150

                validator: IntValidator {
                    bottom: meltdownThresholdSpinBox.from
                    top: meltdownThresholdSpinBox.to
                }

                textFromValue: (value, locale) => {
                    return Number(value).toLocaleString(locale, 'f', 0) + " °C";
                }

                valueFromText: (text, locale) => {
                    return Number.fromLocaleString(locale, text.split(" ")[0]);
                }

                onValueChanged: { cfg_meltdownThreshold = value; }
            }
        }

        Item { Kirigami.FormData.isSection: true }

        QQC2.ButtonGroup { id: primaryLabelGroup }

        QQC2.RadioButton {
            id: primaryLabelTemperatureButton
            Kirigami.FormData.label: "Primary label:"

            text: "Temperature"
            onCheckedChanged: cfg_swapLabels = !checked
        }

        QQC2.RadioButton {
            id: primaryLabelNameButton

            text: "Name"
            onCheckedChanged: cfg_swapLabels = checked
        }

        RowLayout {
            QQC2.Label {
                text: "Font scale:"
            }
            QQC2.SpinBox {
                id: primaryFontScaleSpinBox

                stepSize: toInt(0.1)
                from: toInt(0.0)
                to: toInt(12.0)

                validator: DoubleValidator {
                    bottom: primaryFontScaleSpinBox.from
                    top: primaryFontScaleSpinBox.to
                    decimals: 1
                    notation: DoubleValidator.StandardNotation
                }

                textFromValue: (value, locale) => {
                    return Number(fromInt(value)).toLocaleString(locale, 'f', 1);
                }

                valueFromText: (text, locale) => {
                    return Math.round(toInt(Number.fromLocaleString(locale, text)));
                }

                onValueChanged: { cfg_fontScale = fromInt(value); }

                function toInt(value) {
                    return value * 10;
                }

                function fromInt(value) {
                    return value / 10;
                }
            }
        }

        Item { Kirigami.FormData.isSection: true }

        QQC2.CheckBox {
            id: isoFontBox
            Kirigami.FormData.label: "Secondary label:"

            text: "Same Font as Primary label"
            onCheckedChanged: { cfg_isoFont = checked; }
        }

        QQC2.CheckBox {
            id: secondaryLabelCenteredBox

            text: "Centered"
            onCheckedChanged: { cfg_secondaryLabelCentered = checked; }
        }

        RowLayout {
            QQC2.Label {
                text: "Font scale:"
            }
            QQC2.SpinBox {
                id: secondaryFontScaleSpinBox
                //Kirigami.FormData.label: "Font scale:"

                stepSize: toInt(0.1)
                from: toInt(0.0)
                to: toInt(12.0)

                validator: DoubleValidator {
                    bottom: secondaryFontScaleSpinBox.from
                    top: secondaryFontScaleSpinBox.to
                    decimals: 1
                    notation: DoubleValidator.StandardNotation
                }

                textFromValue: (value, locale) => {
                    return Number(fromInt(value)).toLocaleString(locale, 'f', 1);
                }

                valueFromText: (text, locale) => {
                    return Math.round(toInt(Number.fromLocaleString(locale, text)));
                }

                onValueChanged: { cfg_secondaryFontScale = fromInt(value); }

                function toInt(value) {
                    return value * 10;
                }

                function fromInt(value) {
                    return value / 10;
                }
            }
        }

        RowLayout {
            QQC2.Label {
		    text: "Opacity:"
            }
            QQC2.SpinBox {
                id: secondaryLabelOpacitySpinBox

                stepSize: toInt(0.1)
                from: toInt(0.0)
                to: toInt(1.0)

                validator: DoubleValidator {
                    bottom: secondaryLabelOpacitySpinBox.from
                    top: secondaryLabelOpacitySpinBox.to
                    decimals: 1
                    notation: DoubleValidator.StandardNotation
                }

                textFromValue: (value, locale) => {
                    return Number(fromInt(value)).toLocaleString(locale, 'f', 1);
                }

                valueFromText: (text, locale) => {
                    return Math.round(toInt(Number.fromLocaleString(locale, text)));
                }

                onValueChanged: { cfg_secondaryLabelOpacity = fromInt(value); }

                function toInt(value) {
                    return value * 10;
                }

                function fromInt(value) {
                    return value / 10;
                }
            }
        }

    }
}
