/*
    SPDX-FileCopyrightText: 2023 Oliver Beard <olib141@outlook.com>
    SPDX-License-Identifier: MIT
*/

import QtQuick
import QtQuick.Layouts

import org.kde.plasma.plasmoid
import org.kde.plasma.core as PlasmaCore
import org.kde.plasma.components as PlasmaComponents
import org.kde.kirigami as Kirigami

ColumnLayout {
    id: delegate

    property string name
    property string sensorId

    readonly property double updateRateLimit: Plasmoid.configuration.updateInterval
    readonly property int unit: Plasmoid.configuration.temperatureUnit

    readonly property bool showUnit: Plasmoid.configuration.showUnit
    readonly property bool enableDangerColor: Plasmoid.configuration.enableDangerColor
    readonly property int warningThreshold: Plasmoid.configuration.warningThreshold
    readonly property int meltdownThreshold: Plasmoid.configuration.meltdownThreshold
    readonly property bool swapLabels: Plasmoid.configuration.swapLabels
    readonly property bool isoFont: Plasmoid.configuration.isoFont
    readonly property double primaryFontScale: Plasmoid.configuration.fontScale
    readonly property double secondaryFontScale: Plasmoid.configuration.secondaryFontScale
    readonly property double secondaryLabelOpacity: Plasmoid.configuration.secondaryLabelOpacity
    readonly property double secondaryLabelCentered: Plasmoid.configuration.secondaryLabelCentered

    Connections {
        target: Kirigami.Theme
        function onDefaultFontChanged() { primaryFontScaleChanged(); secondaryFontScaleChanged(); }
        function onSmallFontChanged() { primaryFontScaleChanged(); secondaryFontScaleChanged(); }
    }

    onIsoFontChanged: { secondaryFontScaleChanged(); }

    onPrimaryFontScaleChanged: {
        primaryLabel.font = Kirigami.Theme.defaultFont;
        primaryLabel.font.pointSize = primaryLabel.font.pointSize * primaryFontScale;
    }
    function mylog() {
        console.log("")
        console.log("root.height="+root.height)
        console.log("grid.height="+grid.height)
        console.log("grid.implicitHeight="+grid.implicitHeight)
        console.log("grid.width="+grid.width)
        console.log("grid.implicitWidth="+grid.implicitWidth)
        console.log("delegate.height="+delegate.height)
        console.log("delegate.width="+delegate.width)
        console.log("primaryLabel.height="+primaryLabel.height)
        console.log("primaryLabel.width="+primaryLabel.width)
        console.log("secondaryLabel.height="+secondaryLabel.height)
        console.log("secondaryLabel.width="+secondaryLabel.width)
        console.log("grid.columnSpacing="+grid.columnSpacing)
        console.log("grid.rowSpacing="+grid.rowSpacing)
        console.log("delegate.spacing="+delegate.spacing)
        console.log("Layout.topMargin="+Layout.topMargin)
        console.log("Layout.leftMargin="+Layout.leftMargin)
        console.log("primaryLabel.ContentHeight="+primaryLabel.contentHeight)
        console.log("secondaryLabel.ContentHeight="+secondaryLabel.contentHeight)
        console.log("criteria="+(grid.height)+" "+(primaryLabel.contentHeight+secondaryLabel.contentHeight+delegate.spacing)*0.8)
    }

    onSecondaryFontScaleChanged: {
        secondaryLabel.font = isoFont ? Kirigami.Theme.defaultFont : Kirigami.Theme.smallFont;
        secondaryLabel.font.pointSize =  secondaryLabel.font.pointSize * secondaryFontScale;
    }

    onSecondaryLabelOpacityChanged: { secondaryLabel.opacity=secondaryLabelOpacity; }

    property alias sensor: sensorLoader.item

    onUpdateRateLimitChanged: {
        if (sensor) { sensor.updateRateLimit = updateRateLimit * 1000; }
    }

    Layout.topMargin:   -0.5*rowspacing()
    Layout.leftMargin:  grid.isVertical ? Math.max(0.5,(grid.width+grid.columnSpacing-Math.max(primaryLabel.contentWidth,secondaryLabel.contentWidth))/2) : 0.5 * Kirigami.Units.smallSpacing * Math.max(primaryFontScale,secondaryFontScale)
    Layout.rightMargin: 0.5 * Kirigami.Units.smallSpacing * Math.max(primaryFontScale,secondaryFontScale)
    Layout.alignment:   Qt.AlignHCenter | Qt.AlignVCenter

    spacing: rowspacing()

    function rowspacing() {
        if (grid.isVertical) {
            return 0;
        } else {
            return Math.min(0.0,(grid.height+grid.rowSpacing-primaryLabel.contentHeight-secondaryLabel.contentHeight)/2);
        } 
    }

    Loader {
        id: sensorLoader

        Component.onCompleted: setSource("SensorProxy.qml", {
            "sensorId": sensorId,
            "updateRateLimit": updateRateLimit * 1000
        })
    }

    PlasmaComponents.Label {
        id: primaryLabel

        Layout.alignment: Qt.AlignHCenter

        font: Kirigami.Theme.defaultFont
        text: swapLabels ? delegate.nameText() : delegate.temperatureText()

        color: swapLabels ? PlasmaCore.Theme.textColor : temperatureColor()

        Behavior on color {
            ColorAnimation {
                id: temperatureColorAnimation

                duration: Kirigami.Units.longDuration
                easing.type: Easing.InOutQuad
            }
        }
    }

    PlasmaComponents.Label {
        id: secondaryLabel

        Layout.alignment: secondaryLabelCentered ? Qt.AlignHCenter : Qt.AlignRight

        font: isoFont ? Kirigami.Theme.defaultFont : Kirigami.Theme.smallFont
        text: swapLabels ? delegate.temperatureText() : delegate.nameText()
        opacity: secondaryLabelOpacity
        visible: text && grid.height >= (primaryLabel.contentHeight + contentHeight + delegate.spacing) * 0.8

        color: swapLabels ? temperatureColor() : PlasmaCore.Theme.textColor

        Behavior on color {
            ColorAnimation {
                id: nameColorAnimation

                duration: Kirigami.Units.longDuration
                easing.type: Easing.InOutQuad
            }
        }
    }

    function temperatureText() {
        if (delegate.sensor && delegate.sensor.value !== undefined) {
            switch (delegate.unit) {
                case 0:
                default:
                    return delegate.sensor.value.toFixed(0) + (delegate.showUnit ? " °C" : "");
                case 1:
                    return (delegate.sensor.value * 1.8 + 32).toFixed(0) + (delegate.showUnit ? " °F" : "");
                case 2:
                    return (delegate.sensor.value  + 273.15).toFixed(0) + (delegate.showUnit ? " K" : "");
            }
        } else {
            return "—";
        }
    }

    function nameText() {
        return delegate.name;
    }

    function temperatureColor() {
        if (enableDangerColor && delegate.sensor && delegate.sensor.value !== undefined) {
            let temperature = delegate.sensor.value.toFixed(0);
            if (temperature >= delegate.meltdownThreshold) {
                return PlasmaCore.Theme.negativeTextColor;
            } else if (temperature >= delegate.warningThreshold) {
                return PlasmaCore.Theme.neutralTextColor;
            }
        }

        return PlasmaCore.Theme.textColor;
    }
}
